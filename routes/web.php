<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index')->name('index');

Route::get('formulario-reembolso-paso-1', 'RefundController@createUser')->name('create.user');
Route::post('formulario-reembolso-paso-1', 'RefundController@postCreateUser')->name('create.user.post');

Route::get('formulario-reembolso-paso-2', 'RefundController@createRefund')->name('create.refund');
Route::post('formulario-reembolso-paso-2', 'RefundController@postCreateRefund')->name('create.refund.post');

Route::get('formulario-reembolso-paso-3', 'RefundController@validateRefund')->name('validate.refund');

Route::get('gracias', 'IndexController@validatedRefund')->name('validated.refund');
Route::post('email-confirmación', 'IndexController@postValidatedRefund')->name('validated.refund.post');

Route::get('d7fc3bf278dfe94caafc57e8f048a784/export', 'ExportController@export');
