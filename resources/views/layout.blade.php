<!DOCTYPE html>
<html>
<head>
    <title>Reembolso entradas PortAmérica 2021</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{asset('css/app.css')}}" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@200;300;400;500;600;700&display=swap" rel="stylesheet">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('img/favicon/favicon.ico')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('img/favicon/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('img/favicon/favicon-16x16.png')}}">
    <link rel="manifest" href="/site.webmanifest">
    <script src="{{asset('js/app.js')}}" defer></script>
</head>
<body>
    <nav class="flex flex-wrap items-center justify-between p-5" id="mainNav">
        <div class="flex flex-wrap items-center w-full md:w-1/4 justify-between bg-white px-6 py-4 shadow-lg">
            <a href="{{ route('index') }}"><img src="{{ asset('img/pa22-logo-min.png') }}" alt="PortAmérica 2022 logo"></a>
        </div>
    </nav>

    <div class="container mx-auto px-5 my-10 md:my-20">
        <h2 class="text-center text-bold text-base uppercase text-pink-500 font-display tracking-wide">Reembolso entradas</h2>
        <div class="w-full">
            <img src="{{ asset('img/pa22-vec-min.png') }}" class="mx-auto w-3/4 md:w-1/5">
        </div>
        @yield('content')
    </div>
    @include('popper::assets')
</body>

</html>
