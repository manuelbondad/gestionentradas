@extends('layout')

@section('content')
    <div class="md:w-2/4 mx-auto mt-10 bg-white pt-4 pb-6 px-6 md:px-8 bg-opacity-80 shadow-lg rounded-md">
        <div class="my-5 items-center font-mono">
            <p class="my-4">Si estás aquí significa que habías puesto tus esperanzas en PortAmérica 2021 en un momento que fue muy difícil para todos.</p>
            <p class="my-4">¡Por eso te estamos <strong>infinitamente agradecidos!</strong></p>
            <p class="my-4">Sin embargo, en 2021 tampoco podremos celebrar nuestro décimo aniversario :(</p>
            <p class="my-4">Completa este formulario para obtener el reembolso de tu entrada o guárdala gratis para #PortAmérica2022</p>
        </div>
        <div class="text-center mb-3 pt-3">
            <a href="https://portamerica.es/" class="my-2 px-6 py-3 text-center bg-purple-800 hover:bg-purple-900 text-white transition-colors font-display uppercase tracking-wide w-full md:w-auto block md:inline-block" target="_blank"><span class="mr-2">Quiero estar en PortAmérica 2022</span></a>
            <a href="{{ route('create.user') }}" class="my-2 px-6 py-3 text-center bg-pink-500 hover:bg-pink-600 text-white transition-colors font-display uppercase tracking-wide w-full md:w-auto block md:inline-block"><span class="mr-2">Empezar</span> <i class="fas fa-chevron-right"></i></a>
        </div>
    </div>
@endsection
