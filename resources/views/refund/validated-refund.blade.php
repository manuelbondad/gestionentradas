@extends('layout')

@section('content')
    <div class="sm:w-full lg:w-2/4 mx-auto mt-10 bg-white py-4 px-6 md:px-8 bg-opacity-80 shadow-lg rounded-md font-mono">
        <div class="my-5 items-center">
            <p>Esto no es un adiós. Volveremos a bailar en la Carballeira, a cantar, gritar y a reír sin parar.
                Estamos seguros de ello: Los días buenos están muy cerca.</p><br>
            <p>En 2022 nos esperan en la Carballeira 3 días de música, gastronomía y naturaleza. Prometemos que el décimo aniversario será el mejor PortAmérica de la historia.</p><br>
            <p>Y es que Igual que tú, somos <strong>especialistas en remontadas :)</strong></p><br>
            <p>Hasta entonces toca seguir cuidándose mucho.</p><br>
            <p>#PortAmérica2022. #ÉcheOQueHai.</p>
        </div>
        <div class="text-center mt-10 pb-4">
            <a href="{{ route('index') }}" class="px-6 py-3 text-center bg-purple-800 hover:bg-purple-900 text-white transition-colors font-display uppercase tracking-wide my-3 leading-normal"><span class="mr-2">Volver a inicio</span></a>
        </div>
    </div>
@endsection
