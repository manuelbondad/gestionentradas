@extends('layout')
@section('content')
    <div class="container font-mono">
        <form action="{{ route('create.user.post') }}" method="POST">
            @csrf
            <div class="md:w-2/4 mx-auto mt-10 bg-white pt-6 pb-4 px-6 md:px-8 bg-opacity-80 shadow-lg rounded-md">
                <div class="text-lg"><strong>Paso 1:</strong> Información de compra</div>
                <div class="">
                    <label for="name" class="block my-3">Nombre:</label>
                    <div class="flex justify-between items-center">
                        <input type="text" value="{{ old('name') ?? $user->name ?? ''}}" class="inline-block w-full focus:ring-purple-800 focus:border-purple-800" id="name"  name="name" placeholder="Nombre*">
                        <div class="w-auto inline-block text-right pl-2">
                            <i class="fas fa-info-circle text-purple-800 hover:text-purple-900" {{ Popper::arrow()->pop('Introduce el nombre que figura en comprobante de la compra de las entradas') }}></i>
                        </div>
                    </div>
                    @error('name')
                        <div class="text-red-600"><i class="fas fa-exclamation-triangle"></i> {{ $errors->first('name') }}</div>
                    @enderror
                </div>

                <div class="">
                    <label for="surname" class="block my-3">Apellidos:</label>
                    <div class="flex justify-between items-center">
                        <input type="text" value="{{ old('surname') ?? $user->surname ?? '' }}" class="inline-block w-full focus:ring-purple-800 focus:border-purple-800" id="surname"  name="surname" placeholder="Apellidos*">
                        <div class="w-auto inline-block text-right pl-2">
                            <i class="fas fa-info-circle text-purple-800 hover:text-purple-900" {{ Popper::arrow()->pop('Introduce el/los apellido/s que figura en el comprobante de la compra de las entradas') }}></i>
                        </div>
                    </div>
                    @error('surname')
                    <div class="text-red-600"><i class="fas fa-exclamation-triangle"></i> {{ $errors->first('surname') }}</div>
                    @enderror
                </div>

                <div class="">
                    <label for="email" class="block my-3">Correo electrónico:</label>
                    <div class="flex justify-between items-center">
                        <input type="email" value="{{ old('email') ?? $user->email ?? '' }}" class="inline-block w-full focus:ring-purple-800 focus:border-purple-800" id="email"  name="email" placeholder="ejemplo@email.com">
                        <div class="w-auto inline-block text-right pl-2">
                            <i class="fas fa-info-circle text-purple-800 hover:text-purple-900" {{ Popper::arrow()->pop('Introduce el correo electrónico que figura en el comprobante de la compra de las entradas') }}></i>
                        </div>
                    </div>
                    @error('email')
                    <div class="text-red-600"><i class="fas fa-exclamation-triangle"></i> {{ $errors->first('email') }}</div>
                    @enderror
                </div>

                <div class="">
                    <label for="order_num" class="block my-3">Número de pedido:</label>
                    <div class="flex justify-between items-center">
                        <input type="text" value="{{ old('order_num') ?? $user->order_num ?? '' }}" class="inline-block w-full focus:ring-purple-800 focus:border-purple-800" id="orderNum"  name="order_num" placeholder="1234567890">
                        <div class="w-auto inline-block text-right pl-2">
                            <i class="fas fa-info-circle text-purple-800 hover:text-purple-900" {{ Popper::arrow()->pop('Introduce el número de compra que figura en el comprobante de la compra de las entradas') }}></i>
                        </div>
                    </div>
                    @error('order_num')
                    <div class="text-red-600"><i class="fas fa-exclamation-triangle"></i> {{ $errors->first('order_num') }}</div>
                    @enderror
                </div>

                <div class="">
                    <label for="ticket_qty" class="block my-3">Número de entradas compradas:</label>
                    <div class="flex justify-between items-center">
                        <input type="number" value="{{ old('ticket_qty') ?? $user->ticket_qty ?? '' }}" class="inline-block w-full focus:ring-purple-800 focus:border-purple-800" id="ticketQty"  name="ticket_qty" placeholder="0">
                        <div class="w-auto inline-block text-right pl-2">
                            <i class="fas fa-info-circle text-purple-800 hover:text-purple-900" {{ Popper::arrow()->pop('Introduce el número de entradas que figura en el comprobante de la compra de las entradas') }}></i>
                        </div>
                    </div>
                    @error('ticket_qty')
                    <div class="text-red-600"><i class="fas fa-exclamation-triangle"></i> {{ $errors->first('ticket_qty') }}</div>
                    @enderror
                </div>
                <div class="text-right mt-3 pt-3">
                    <a href="{{ route('index') }}" class="px-6 py-3 text-center bg-purple-800 hover:bg-purple-900 text-white transition-colors font-display uppercase tracking-wide my-3 leading-normal w-full md:w-auto block md:inline-block">Cancelar</a>
                    <button type="submit" class="px-6 py-3 text-center bg-pink-500 hover:bg-pink-600 text-white transition-colors font-display uppercase tracking-wide my-3 leading-normal w-full md:w-auto block md:inline-block"><span class="mr-2">Siguiente</span> <i class="fas fa-chevron-right"></i></button>
                </div>
            </div>
        </form>
    </div>
@endsection
