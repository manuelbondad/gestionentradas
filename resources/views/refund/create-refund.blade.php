@extends('layout')
@section('content')
    <div class="container font-mono">
        <form action="{{ route('create.refund.post') }}" method="POST">
            @csrf
            <div class="md:w-2/4 mx-auto mt-10 bg-white pb-4 pt-6 px-6 md:px-8 bg-opacity-80 shadow-lg rounded-md">
                <div class="text-lg"><strong>Paso 2:</strong> Número de entradas</div>
                @error('qty')
                    <div class="text-red-600"><i class="fas fa-exclamation-triangle"></i> {{ $errors->first('qty') }}</div>
                @enderror
                <div class="">
                    <label for="pa_type" class="block my-3">Tipo de ABONO:</label>
                    <div class="flex justify-between items-center">
                        <select value="{{ $refund->pa_type ?? '' }}" class="inline-block w-full focus:ring-purple-800 focus:border-purple-800" id="paType"  name="pa_type">
                            <option value="" default>Seleccione una opción</option>
                            <option value="pa_pass_01">Abono CREYENTES</option>
                            <option value="pa_pass_02">Abono | 2ª PROMOCIÓN DE LANZAMIENTO</option>
                            <option value="pa_pass_03">Abono | 3ª PROMOCIÓN DE LANZAMIENTO</option>
                            <option value="pa_pass_04">Abono | 4ª PROMOCIÓN DE LANZAMIENTO</option>
                        </select>
                        <div class="w-auto inline-block text-right pl-2">
                            <i class="fas fa-info-circle text-purple-800 hover:text-purple-900" {{ Popper::arrow()->pop('En caso de haber comprado un ABONO, selecciona el tipo de abono adquirido') }}></i>
                        </div>
                </div>
                    @error('pa_type')
                    <div class="text-red-600"><i class="fas fa-exclamation-triangle"></i> {{ $errors->first('pa_type') }}</div>
                    @enderror
                </div>

                <div class="">
                    <label for="pa_pass" class="block my-3">Número de entradas ABONO:</label>
                    <div class="flex justify-between items-center">
                        <input type="number" value="{{ $refund->pa_pass ?? '' }}" class="inline-block w-full focus:ring-purple-800 focus:border-purple-800" id="paPass"  name="pa_pass" placeholder="Selecciona una cantidad">
                        <div class="w-auto inline-block text-right pl-2">
                            <i class="fas fa-info-circle text-purple-800 hover:text-purple-900" {{ Popper::arrow()->pop('Introduce el número de ABONOS a devolver') }}></i>
                        </div>
                    </div>
                    @error('pa_pass')
                    <div class="text-red-600"><i class="fas fa-exclamation-triangle"></i> {{ $errors->first('pa_pass') }}</div>
                    @enderror
                </div>

                <hr class="mt-6">

                <div class="">
                    <label for="pa_th" class="block my-3">Número de entradas JUEVES:</label>
                    <div class="flex justify-between items-center">
                        <input type="number" value="{{ $refund->pa_th ?? '' }}" class="inline-block w-full focus:ring-purple-800 focus:border-purple-800" id="paTh"  name="pa_th" placeholder="Selecciona una cantidad">
                        <div class="w-auto inline-block text-right pl-2">
                            <i class="fas fa-info-circle text-purple-800 hover:text-purple-900" {{ Popper::arrow()->pop('Introduce número de Entradas de JUEVES a devolver') }}></i>
                        </div>
                    </div>
                    @error('pa_th')
                    <div class="text-red-600"><i class="fas fa-exclamation-triangle"></i> {{ $errors->first('pa_th') }}</div>
                    @enderror
                </div>

                <hr class="mt-6">

                <div class="">
                    <label for="pa_fr" class="block my-3">Número de entradas VIERNES:</label>
                    <div class="flex justify-between items-center">
                        <input type="number" value="{{ $refund->pa_fr ?? '' }}" class="inline-block w-full focus:ring-purple-800 focus:border-purple-800" id="paFr"  name="pa_fr" placeholder="Selecciona una cantidad">
                        <div class="w-auto inline-block text-right pl-2">
                            <i class="fas fa-info-circle text-purple-800 hover:text-purple-900" {{ Popper::arrow()->pop('Introduce el número de Entradas de VIERNES a devolver') }}></i>
                        </div>
                    </div>
                    @error('pa_fr')
                    <div class="text-red-600"><i class="fas fa-exclamation-triangle"></i> {{ $errors->first('pa_fr') }}</div>
                    @enderror
                </div>

                <hr class="mt-6">

                <div class="">
                    <label for="pa_sa" class="block my-3">Número de entradas SÁBADO:</label>
                    <div class="flex justify-between items-center">
                        <input type="number" value="{{ $refund->pa_sa ?? '' }}" class="inline-block w-full focus:ring-purple-800 focus:border-purple-800" id="paSa"  name="pa_sa" placeholder="Selecciona una cantidad">
                        <div class="w-auto inline-block text-right pl-2">
                            <i class="fas fa-info-circle text-purple-800 hover:text-purple-900" {{ Popper::arrow()->pop('Introduce el número de Entradas de SÁBADO a devolver') }}></i>
                        </div>
                    </div>
                    @error('pa_sa')
                    <div class="text-red-600"><i class="fas fa-exclamation-triangle"></i> {{ $errors->first('pa_sa') }}</div>
                    @enderror
                </div>

                <hr class="mt-6">

                <div class="">
                    <label for="pa_cam" class="block my-3">Número de entradas ACAMPADA:</label>
                    <div class="flex justify-between items-center">
                        <input type="number" value="{{ $refund->pa_cam ?? '' }}" class="inline-block w-full focus:ring-purple-800 focus:border-purple-800" id="paCam"  name="pa_cam" placeholder="Selecciona una cantidad">
                        <div class="w-auto inline-block text-right pl-2">
                            <i class="fas fa-info-circle text-purple-800 hover:text-purple-900" {{ Popper::arrow()->pop('Introduce el número de Entradas de ACAMPADA a devolver') }}></i>
                        </div>
                    </div>
                    @error('pa_cam')
                    <div class="text-red-600"><i class="fas fa-exclamation-triangle"></i> {{ $errors->first('pa_cam') }}</div>
                    @enderror
                </div>
                <div class="text-right mt-3 pt-3">
                    <a href="{{ route('create.user') }}" class="px-6 py-3 text-center bg-purple-800 hover:bg-purple-900 text-white transition-colors font-display uppercase tracking-wide my-3 leading-normal w-full md:w-auto block md:inline-block"><i class="fas fa-chevron-left"></i><span class="ml-2">Atrás</span></a>
                    <button type="submit" class="px-6 py-3 text-center bg-pink-500 hover:bg-pink-600 text-white transition-colors font-display uppercase tracking-wide my-3 leading-normal w-full md:w-auto block md:inline-block"><span class="mr-2">Siguiente</span> <i class="fas fa-chevron-right"></i></button>
                </div>
            </div>
        </form>
    </div>
@endsection
