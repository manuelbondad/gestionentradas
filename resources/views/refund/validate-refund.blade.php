@extends('layout')

@section('content')
    <form action="{{ route('validated.refund.post') }}" method="POST">
        @csrf
        <div class="md:w-2/4 mx-auto mt-10 bg-white py-4 px-8 bg-opacity-80 shadow-lg rounded-md font-mono">
            <div class="my-5">
                <strong>ALTO.</strong> Comprueba que los datos que aparecen a continuación son correctos antes de continuar. ¡Ya casi hemos terminado!
            </div>
            <div class="my-5">
                <div><span class="font-bold">Nombre:</span> {{$user->name}}</div>
                <div><span class="font-bold">Apellidos:</span> {{$user->surname}}</div>
                <div><span class="font-bold">Email:</span> {{$user->email}}</div>
                <div><span class="font-bold">Número de pedido:</span> {{$user->order_num}}</div>
                <div><span class="font-bold">Número de entradas compradas:</span> {{$user->ticket_qty}}</div>
                @if(!empty($refund->pa_type))
                    <div><span class="font-bold">Tipo de ABONO a devolver:</span>
                        @switch($refund->pa_type)
                            @case('pa_pass_01')
                                Abono CREYENTES
                                @break
                            @case('pa_pass_02')
                                Abono | 2ª PROMOCIÓN DE LANZAMIENTO
                                @break
                            @case('pa_pass_03')
                                Abono | 3ª PROMOCIÓN DE LANZAMIENTO
                                @break
                            @case('pa_pass_04')
                                Abono | 4ª PROMOCIÓN DE LANZAMIENTO
                                @break
                            @default
                                Abono 3 días
                        @endswitch
                    </div>
                @endif
                @if(!empty($refund->pa_pass))
                    <div><span class="font-bold">Entradas ABONO a devolver:</span> {{$refund->pa_pass}}</div>
                @endif
                @if(!empty($refund->pa_th))
                    <div><span class="font-bold">Entradas JUEVES a devolver:</span> {{$refund->pa_th}}</div>
                @endif
                @if(!empty($refund->pa_fr))
                    <div><span class="font-bold">Entradas VIERNES a devolver:</span> {{$refund->pa_fr}}</div>
                @endif
                @if(!empty($refund->pa_sa))
                    <div><span class="font-bold">Entradas SÁBADO a devolver:</span> {{$refund->pa_sa}}</div>
                @endif
                @if(!empty($refund->pa_cam))
                    <div><span class="font-bold">Entradas ACAMPADA a devolver:</span> {{$refund->pa_cam}}</div>
                @endif
            </div>
            <div>
                <input type="checkbox" name="terms" class="text-pink-500 focus:ring-pink-500"> <span class="text-sm">He leído y acepto la <a href="https://portamerica.es/politica-de-privacidad/" target="_blank" class="hover:text-purple-800">política de privacidad</a></span>.
                @error('terms')
                <div class="text-red-600 text-sm"><i class="fas fa-exclamation-triangle"></i> {{ $errors->first('terms') }}</div>
                @enderror
            </div>
            <div class="text-right mt-8">
                <a href="{{ route('create.refund') }}" class="px-6 py-3 text-center bg-purple-800 hover:bg-purple-900 text-white transition-colors font-display uppercase tracking-wide my-3 leading-normal w-full md:w-auto block md:inline-block"><i class="fas fa-chevron-left"></i><span class="ml-2">Atrás</span></a>
                <button type="submit" class="px-6 py-3 text-center bg-pink-500 hover:bg-pink-600 text-white transition-colors font-display uppercase tracking-wide my-3 leading-normal w-full md:w-auto block md:inline-block">Confirmar</button>
            </div>
        </div>
    </form>
@endsection
