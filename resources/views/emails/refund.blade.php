<div class="mb-3">
    <p>¡Saludos {{ $user->name }}!</p>
    <p>Agradecemos sinceramente tu paciencia y apoyo.</p>
    <p>Si cambias de opinión te esperamos en 2022, cuando por fin celebraremos nuestro décimo aniversario. <strong>¡Estamos seguros de que será el mejor PortAmérica de la historia!</strong>.</p>
    <p>Y <strong>atención a nuestra redes</strong>: si la evolución de la pandemia es favorable, es muy probable que pasado el verano nos podamos reencontrar en la Carballeira. Ya estamos trabajando en una programación adaptada a las circunstancias que aunará, cómo no, música, gastronomía y naturaleza.</p>
    <p class="font-bold">#PortAmérica2022.</p>
    <p class="font-bold">#EspecialistasEnRemontadas.</p>
    <p class="font-bold"><a href="https://www.youtube.com/watch?v=sZNlFJkzsQk" target="_blank">#EsaMalditaFelicidad</a></p>
</div>
<br>
<hr class="mb-3">
<br>
<div class="mb-3">
    <p>Tu solicitud ha sido registrada.</p>
    <p>A continuación te indicamos los datos que hemos registrado:</p>
    <p><strong>- Nombre:</strong> {{$user->name}}</p>
    <p><strong>- Apellidos:</strong> {{$user->surname}}</p>
    <p><strong>- Email:</strong> {{$user->email}}</p>
    <p><strong>- Número de pedido:</strong> {{$user->order_num}}</p>
    <p><strong>- Número de entradas compradas:</strong> {{$user->ticket_qty}}</p>
    <p><strong>- Número de entradas a devolver: </strong></p>
    <ul>
        @if(!empty($refund->pa_pass))
            @switch($refund->pa_type)
                @case('pa_pass_01')
                <li><strong>Abono CREYENTES:</strong> {{$refund->pa_pass}}</li>
                @break
                @case('pa_pass_02')
                <li><strong>Abono | 2ª PROMOCIÓN DE LANZAMIENTO:</strong> {{$refund->pa_pass}}</li>
                @break
                @case('pa_pass_03')
                <li><strong>Abono | 3ª PROMOCIÓN DE LANZAMIENTO:</strong> {{$refund->pa_pass}}</li>
                @break
                @case('pa_pass_04')
                <li><strong>Abono | 4ª PROMOCIÓN DE LANZAMIENTO:</strong> {{$refund->pa_pass}}</li>
                @break
                @default
                Abono 3 días:
            @endswitch
        @endif
        @if(!empty($refund->pa_th))
            <li><strong>Entradas JUEVES:</strong> {{$refund->pa_th}}</li>
        @endif
        @if(!empty($refund->pa_fr))
            <li><strong>Entradas VIERNES:</strong> {{$refund->pa_fr}}</li>
        @endif
        @if(!empty($refund->pa_sa))
            <li><strong>Entradas SÁBADO:</strong> {{$refund->pa_sa}}</li>
        @endif
        @if(!empty($refund->pa_cam))
            <li><strong>Entradas ACAMPADA:</strong> {{$refund->pa_cam}}</li>
        @endif
    </ul>
</div>
<br>
<hr class="mb-3">
<br>
<div class="mb-3">
    <p>Entre el día 25 de mayo y como máximo hasta el día 7 de junio, recibirás la devolución solicitada en el mismo medio de pago (tarjeta o PayPal) utilizado para realizar la compra original. Si el medio de pago ya no está operativo, debes hablar con la entidad emisora para que pueda redirigir la devolución a otro de tu misma titularidad.</p>
</div>
<br>
<div class="mb-3">Saludos.</div>
<br>
<div class="mb-3">El equipo PortAmérica.</div>
<br>
<div style="width: 100%;" class="mb-3">
    <img style="width: 100%" src="https://portamerica.es/uploads/pa22-header.jpeg" alt="PortAmérica 2022"/>
</div>
<br>
<div class="mb-3 text-sm">
    <p><strong>Información sobre el tratamiento de sus datos personales de acuerdo con lo establecido en el Reglamento (UE) 2016/679, de 27 de abril, (RGPD) y en la L.O. 3/2018, de 5 de diciembre, de protección de datos y garantía de los derechos digitales (LOPDGDD)</strong></p>
    <p>Información resumida sobre Protección de Datos:</p>
    <ul>
        <li><strong>Responsable de los datos:</strong> ESMERARTE INDUSTRIAS CREATIVAS, S.L.U. … <a href="https://portamerica.es/aviso-legal-condiciones-uso/" target="_blank">Más información</a></li>
        <li><strong>Finalidad:</strong> Solo recogeremos sus datos personales para los fines declarados y solo según sus deseos. … <a href="https://portamerica.es/politica-de-privacidad/" target="_blank">Más información</a></li>
        <li><strong>Destinatarios:</strong> Sus datos no van a ser cedidos a terceros, salvo que exista una obligación legal.</li>
        <li><strong>Legitimación:</strong> Consentimiento de la persona interesada o interés legítimo. … <a href="https://portamerica.es/politica-de-privacidad/" target="_blank">Más información</a></li>
        <li><strong>Derechos:</strong> Tiene derecho a acceder, rectificar, cancelar y suprimir sus datos, así como otros derechos que le asisten, dirigiendo comunicación motivada y acreditada en info@esmerarte.com … <a href="https://portamerica.es/aviso-legal-condiciones-uso/">Más información</a></li>
        <li><strong>Información adicional:</strong> Puede consultar esta información más detallada y otra información adicional en nuestra política de privacidad <a href="https://portamerica.es/politica-de-privacidad/" target="_blank">aquí</a>.</li>
    </ul>
</div>
