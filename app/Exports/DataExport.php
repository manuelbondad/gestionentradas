<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class DataExport implements FromCollection, WithStrictNullComparison
{
    public function collection()
    {
        return DB::table('users')
        ->join('refunds', 'users.order_num', '=' , 'refunds.users_order_num')
        ->select('users.id', 'users.order_num', 'users.created_at', 'users.name', 'users.surname', 'users.email', 'refunds.pa_pass_01', 'refunds.pa_pass_02', 'refunds.pa_pass_03', 'refunds.pa_pass_04', 'refunds.pa_th', 'refunds.pa_fr', 'refunds.pa_sa', 'refunds.pa_cam', 'users.ticket_qty')
        ->get();
    }
}
