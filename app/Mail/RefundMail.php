<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RefundMail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $refund;

    public function __construct($user, $refund)
    {
        $this->user = $user;
        $this->refund = $refund;
    }

    public function build()
    {
        return $this->subject('Reembolso entradas PortAmérica 2021')->view('emails.refund');
    }
}
