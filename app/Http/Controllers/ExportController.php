<?php

namespace App\Http\Controllers;

use App\Exports\DataExport;
use Maatwebsite\Excel\Facades\Excel;


class ExportController extends Controller
{
    public function export()
    {
        return Excel::download(new DataExport(), 'reembolso-2021.xlsx');
    }
}
