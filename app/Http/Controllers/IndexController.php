<?php

namespace App\Http\Controllers;

use App\Mail\RefundMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{
    public function index(Request $request){
        $request->session()->flush();
        return view('index');
    }

    public function validatedRefund(){
        return view('refund.validated-refund');
    }

    public function postValidatedRefund(Request $request){
        $request->validate([
            'terms' => 'required',
        ]);

        $user = $request->session()->get('user');
        $refund = $request->session()->get('refund');
        if(!empty($user) && !empty($refund)){
            $address = $user->email;

            $user->save();

            $qry = 'insert into refunds (users_order_num';
            $value = 'values (?';
            $values = array();

            if(!empty($refund->pa_type)){
                $qry .= ', '.$refund->pa_type;
                $value .= ', ?';
            }

            array_push($values, $refund->users_order_num);

            if(!empty($refund->pa_pass)){
                array_push($values, $refund->pa_pass);
            }

            if(!empty($refund->pa_th)){
                $qry .= ', pa_th';
                $value .= ', ?';
                array_push($values, $refund->pa_th);
            }

            if(!empty($refund->pa_fr)){
                $qry .= ', pa_fr';
                $value .= ', ?';
                array_push($values, $refund->pa_fr);
            }

            if(!empty($refund->pa_sa)){
                $qry .= ', pa_sa';
                $value .= ', ?';
                array_push($values, $refund->pa_sa);
            }

            if(!empty($refund->pa_cam)){
                $qry .= ', pa_cam';
                $value .= ', ?';
                array_push($values, $refund->pa_cam);
            }

            $qry .= ')';
            $value .= ')';
            $qry .= $value;

            DB::insert(
                $qry, $values
            );

            Mail::to($address)->send(new RefundMail($user,$refund));

            $request->session()->flush();
        }
        return redirect()->route('validated.refund');
    }

}
