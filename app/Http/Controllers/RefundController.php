<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Refund;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class RefundController extends Controller
{
    public function index(){
        return view('refund.index');
    }

    public function createUser(Request $request){
        $user = $request->session()->get('user');
        return view('refund.create-user', compact('user'));
    }

    public function postCreateUser(Request $request){
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'surname' => 'required|max:255',
            'email' => 'required|email:rfc,dns|max:255',
            'order_num' => 'required|numeric|unique:users|digits_between:9,10',
            'ticket_qty' => 'required|numeric|gt:0'
        ]);

        if(empty($request->session()->get('user'))){
            $user = new User();
        }else{
            $user = $request->session()->get('user');
        }

        $user->fill($validatedData);
        $request->session()->put('user', $user);

        return redirect()->route('create.refund');

    }

    public function createRefund(Request $request){
        $refund = $request->session()->get('refund');
        return view('refund.create-refund', compact('refund'));
    }

    public function postCreateRefund(Request $request){

        $user = $request->session()->get('user');

        if(empty($user)) return redirect()->route('create.user');

        $validatedData = $request->validate([
            'pa_type' => 'nullable|in:pa_pass_01,pa_pass_02,pa_pass_03,pa_pass_04|required_with:pa_pass',
            'pa_pass' => 'nullable|numeric|min:0|required_without_all:pa_th,pa_fr,pa_sa,pa_cam|required_with:pa_type',
            'pa_th' => 'nullable|numeric|min:0|required_without_all:pa_pass,pa_fr,pa_sa,pa_cam',
            'pa_fr' => 'nullable|numeric|min:0|required_without_all:pa_th,pa_pass,pa_sa,pa_cam',
            'pa_sa' => 'nullable|numeric|min:0|required_without_all:pa_th,pa_fr,pa_pass,pa_cam',
            'pa_cam' => 'nullable|numeric|min:0|required_without_all:pa_th,pa_fr,pa_pass,pa_sa'
        ]);

        if(array_sum($request->all()) > $user->ticket_qty){
            return redirect()->back()->withErrors(['qty'=> 'El número de entradas a devolver no concuerda con el número de entradas compradas. Por favor, revisa los datos introducidos.']);
        }

        if(empty($request->session()->get('user'))){
            return redirect()->route('create.user');
        }else{
            $user = $request->session()->get('user');
            if(empty($request->session()->get('refund'))){
                $refund = new Refund();
            }else{
                $refund = $request->session()->get('refund');
            }

            $refund->fill($validatedData);
            $refund['users_order_num'] = $user['order_num'];
            $request->session()->put('refund', $refund);
        }

        return redirect()->route('validate.refund');

    }

    public function validateRefund(Request $request){
        $user = $request->session()->get('user');
        $refund = $request->session()->get('refund');
        return view('refund.validate-refund', compact('refund','user'));
    }

}
