<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Refund extends Model
{
    use HasFactory, Notifiable;

    protected $fillable = [
        'users_order_num',
        'pa_type',
        'pa_pass',
        'pa_th',
        'pa_fr',
        'pa_sa',
        'pa_cam',
    ];
}
