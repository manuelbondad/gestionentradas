<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRefundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('refunds', function (Blueprint $table) {
            $table->id();
            $table->string("users_order_num");
            $table->foreign('users_order_num')->references('order_num')->on('users');
            $table->integer('pa_pass_01')->nullable()->default('0');
            $table->integer('pa_pass_02')->nullable()->default('0');
            $table->integer('pa_pass_03')->nullable()->default('0');
            $table->integer('pa_pass_04')->nullable()->default('0');
            $table->integer('pa_th')->nullable()->default('0');
            $table->integer('pa_fr')->nullable()->default('0');
            $table->integer('pa_sa')->nullable()->default('0');
            $table->integer('pa_cam')->nullable()->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('refunds');
    }
}
